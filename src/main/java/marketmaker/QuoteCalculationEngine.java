package marketmaker;

public class QuoteCalculationEngine implements IQuoteCalculationEngine {

    private final static double SPREAD_BPS = 0.0001;

    public double calculateQuotePrice(int securityId, double referencePrice, boolean isBuy, int quantity) {

        double spread = SPREAD_BPS * Math.log(quantity);
        // assumption here is the large quantity the more spread to shield us from price change.
        // logarithm function is used to ensure increase of spread change is decreased
        return isBuy ? referencePrice + spread : referencePrice - spread;
    }
}
