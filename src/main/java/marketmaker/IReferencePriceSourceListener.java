package marketmaker;

import java.util.Map;

/**
 * Callback interface for {@link IReferencePriceSource}
 */
public interface IReferencePriceSourceListener {

    /**
     * Called when a price has changed.
     *
     * @param securityId security identifier
     * @param price      reference price
     */
    void referencePriceChanged(int securityId, double price);

    Map<Integer, Double> getQuoteMap();
}
