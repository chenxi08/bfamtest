package marketmaker;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ReferencePriceSourceListener implements IReferencePriceSourceListener {

    private Map<Integer, Double> quoteMap;
    private final String id;

    public ReferencePriceSourceListener() {
        this.id = UUID.randomUUID().toString();
        this.quoteMap = new ConcurrentHashMap<>();
    }

    public void referencePriceChanged(int securityId, double price) {
        quoteMap.put(securityId, price);
    }

    public Map<Integer, Double> getQuoteMap() {
        return quoteMap;
    }

    public String getId() {
        return id;
    }
}
