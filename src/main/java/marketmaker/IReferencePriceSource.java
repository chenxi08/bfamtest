package marketmaker;

/**
 * Source for reference prices.
 */
public interface IReferencePriceSource {
    /**
     * Subscribe to changes to reference prices.
     *
     * @param listener callback interface for changes
     */
    void subscribe(IReferencePriceSourceListener listener);

    double get(int securityId);


}
