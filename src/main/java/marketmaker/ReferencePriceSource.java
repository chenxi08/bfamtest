package marketmaker;

import util.Constant;

import java.util.Random;

public class ReferencePriceSource implements IReferencePriceSource, Runnable {

    private IReferencePriceSourceListener listener;
    private Random random;

    public ReferencePriceSource() {
        this.random = new Random();
    }

    public void subscribe(IReferencePriceSourceListener listener) {
        this.listener = listener;
    }

    public double get(int securityId) {
        return listener.getQuoteMap().getOrDefault(securityId, -1.0);
    }

    public void run() {
        while (true) {
            if (random.nextBoolean()) {
                // not necessarily generate next price
                double price = random.nextInt(Constant.PRICE_HIGH - Constant.PRICE_LOW)
                        + Constant.PRICE_LOW + random.nextDouble();

                var idSet = Constant.SECURITY_ID_SET;
                var size = idSet.size();
                int id = (int) idSet.toArray()[random.nextInt(size)];
                listener.referencePriceChanged(id, price);
            }
        }
    }
}
