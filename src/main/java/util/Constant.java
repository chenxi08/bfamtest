package util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Constant {
    public static final int PORT_NUMBER = 6464;
    public static final String PORT_HOST = "localhost";
    public static final int UTILITY_THREAD_NUMBER = 10;

    public static final Set<Integer> SECURITY_ID_SET = new HashSet<>(Arrays.asList(101, 102, 103, 104));

    public static final int PRICE_HIGH = 120;
    public static final int PRICE_LOW = 90;

    public static final Set<String> DIRECTION = new HashSet<>(Arrays.asList("buy", "sell"));
}
