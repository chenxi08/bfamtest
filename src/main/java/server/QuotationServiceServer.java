package server;

import marketmaker.QuoteCalculationEngine;
import marketmaker.ReferencePriceSource;
import marketmaker.ReferencePriceSourceListener;
import util.Constant;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executors;

public class QuotationServiceServer {

    public static void main(String[] args) throws Exception {
        var pool = Executors.newFixedThreadPool(Constant.UTILITY_THREAD_NUMBER);
        try (var server = new ServerSocket(Constant.PORT_NUMBER)) {
            System.out.println("The quotation service server is running...");

            var refPriceSource = new ReferencePriceSource();
            var refPriceListener = new ReferencePriceSourceListener();
            var quoteCalcEngine = new QuoteCalculationEngine();

            refPriceSource.subscribe(refPriceListener);

            while (true) {
                pool.execute(refPriceSource);
                pool.execute(new QuotationService(server.accept(), quoteCalcEngine, refPriceSource));
            }
        }finally{
            pool.shutdown();
        }
    }


    private static class QuotationService implements Runnable {

        private Socket socket;
        private QuoteCalculationEngine engine;
        private ReferencePriceSource source;

        QuotationService(Socket socket, QuoteCalculationEngine engine, ReferencePriceSource source) {
            this.socket = socket;
            this.engine = engine;
            this.source = source;
        }

        public void run() {
            System.out.println("Client Connected:  " + socket);

            try {
                var in = new Scanner(socket.getInputStream());
                var out = new PrintWriter(socket.getOutputStream(), true);
                while (in.hasNextLine()) {
                    String[] commands = in.nextLine().split(" ");
                    int securityId = Integer.parseInt(commands[0]);
                    boolean validDirection = Constant.DIRECTION.contains(commands[1].toLowerCase());
                    int quantity = Integer.parseInt(commands[2]);
                    double refPrice = source.get(securityId);
                    if (refPrice <= 0 && !validDirection && quantity < 1) {
                        System.out.println("Please check the input, it is not valid");
                        continue;
                    }
                    boolean isBuy = commands[1].toLowerCase().equals("buy");
                    double quote = engine.calculateQuotePrice(securityId, refPrice, isBuy, quantity);
                    out.println(quote);
                }
            } catch (Exception e) {
                System.out.println("Something wrong with the job");
            } finally {
                try {
                    socket.close();
                } catch (IOException ioe) {
                    System.out.println("Something wrong on closing socket");
                }
                System.out.println("QuotationService socket closed");
            }
        }
    }
}
