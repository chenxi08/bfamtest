package client;

import util.Constant;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class QuotationServiceClient {

    public static void main(String[] args) throws Exception {

        try (var socket = new Socket(Constant.PORT_HOST, Constant.PORT_NUMBER)) {
            System.out.println("Please enter the quote requests: {security ID} (BUY|SELL) {quantity}");
            var scanner = new Scanner(System.in);
            var in = new Scanner(socket.getInputStream());
            var out = new PrintWriter(socket.getOutputStream(), true);
            while (scanner.hasNextLine()) {
                out.println(scanner.nextLine());
                System.out.println("Response of request: " + in.nextLine());
            }
        }
    }


}
