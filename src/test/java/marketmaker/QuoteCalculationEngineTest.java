package marketmaker;

import static org.junit.jupiter.api.Assertions.*;

class QuoteCalculationEngineTest {

    @org.junit.jupiter.api.Test
    void testCalculateQuotePrice() {

        QuoteCalculationEngine engine = new QuoteCalculationEngine();
        int securityId = 123;
        int referencePrice = 100;
        boolean isBuy = true;
        int quantity = 100;

        System.out.println();
        assertEquals(100.0004605170186, engine.calculateQuotePrice(securityId, referencePrice, isBuy, quantity));

    }
}